;;; init-denote.el --- denote customizations -*- lexical-binding: t -*-

;; Author: Andrew Scott <git at andyscott dot me>
;; Keywords: denote, convenience, tools
;; URL: https://codeberg.org/andyscott/dotfiles

;; This file is not part of GNU Emacs

;; Copyright (c) 2024 Andrew Scott

;; MIT No Attribution

;; Permission is hereby granted, free of charge, to any person obtaining a copy of this
;; software and associated documentation files (the "Software"), to deal in the Software
;; without restriction, including without limitation the rights to use, copy, modify,
;; merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
;; INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
;; PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;; SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:

;; My denote customizations.

;;; Code:

;;;; Denote

(use-package denote
  :ensure
  (denote :files (:defaults "denote.*el"))
  :custom
  (denote-directory (expand-file-name "notes" org-directory))
  (denote-known-keywords '("emacs" "projects") "Change default keywords")
  ;; TODO: Make a decision about links
  ;; (denote-org-store-link-to-heading 'context "Store denote links with headings")
  (denote-prompts '(file-type title keywords) "Add `file-type'")
  (denote-templates `((algo-practice . ,(concat "#+url:\n"
                                                "#+difficulty:\n"
                                                "#+topics:\n\n"
                                                "* Tasks\n\n"
                                                "* Input\n\n"
                                                "* Notes\n\n"
                                                "* Solution"))) "Add templates")
  :bind
  ;; (:map dired-mode-map
  ;;       ("" . denote-dired-rename-marked-files-using-front-matter)
  ;;       ("" . denote-dired-rename-marked-files-with-keywords)
  ;;       ("" . denote-dired-rename-files))
  ;; (:map org-mode-map
  ;;       ("" . denote-org-extras-backlinks-for-heading)
  ;;       ("" . denote-org-extras-dblock-insert-links)
  ;;       ("" . denote-org-extras-dblock-insert-backlinks)
  ;;       ("" . denote-org-extras-link-to-heading) ; Prompt for file/heading, add link at point
  ;;       ("" . denote-org-store-link-to-heading) ; Create CUSTOM_ID to heading and store link
  ;;       )
  (:prefix-map as/denote-prefix-map
               :prefix "C-c d")
  (:map as/denote-prefix-map
        ;; Creating notes
        ("n" . denote)
        ("r" . denote-region)
        ("t" . denote-template)
        ;; Dired
        ("d" . denote-sort-dired)
        ;; Opening/creating
        ("f" . denote-open-or-create)
        ("F" . denote-open-or-create-with-command)
        ;; Linking
        ("a" . denote-add-links)           ; Link to all notes matching a regex
        ("b" . denote-backlinks)             ; Create a buffer with backlinks
        ("l" . denote-link)                ; Insert a link to another note
        ("L" . denote-link-or-create)      ; Link to the target file, creating the file if needed
        ("C-l" . denote-link-after-creating) ; Create a file in the background, then link to it in the current file
        ("C-f" . denote-find-backlink)       ; Use minibuffer completion to find backlinks
        ;; Renaming
        ("C" . denote-change-file-type-and-front-matter)
        ("R" . denote-rename-file)
        ;; Silos
        ("s n" . denote-silo-extras-create-note)
        ("s f" . denote-silo-extras-open-or-create)
        ("s F" . denote-silo-extras-select-silo-then-command))
  :hook
  (dired-mode . denote-dired-mode)
  (text-mode . denote-fontify-links-mode-maybe)
  :config
  (denote-rename-buffer-mode))

;;;;; Consult Notes

(use-package consult-notes
  :custom
  (consult-notes-file-dir-sources
   `(("Org"       ?o ,org-directory)
     ("Projects"  ?r ,(expand-file-name "projects" org-directory))) "Note locations w/ narrowing key")
  ;; (consult-notes-org-headings-files '("~/path/to/file1.org"
  ;;                                     "~/path/to/file2.org") "Files for `consult-org-headings-mode'")
  :bind
  (:map as/denote-prefix-map
        ("c" . consult-notes)
        ("a" . consult-notes-search-in-all-notes))
  :config
  ;; (consult-notes-org-headings-mode)
  (consult-notes-denote-mode))



;;;;; Citar + Denote

(use-package citar-denote
  :if
  (and (elpaca-installed-p 'denote)
       (elpaca-installed-p 'citar))
  :custom
  (citar-denote-keyword "bib")
  (citar-denote-file-type 'org)
  (citar-denote-title-format-authors 2)
  :bind
  (:map as/denote-prefix-map
        ("C-c a" . citar-denote-add-citekey)
        ("C-c d" . citar-denote-dwim)
        ("C-c f" . citar-denote-open-note))
  :config
  (citar-denote-mode))

(provide 'init-denote)
;;; init-denote.el ends here
