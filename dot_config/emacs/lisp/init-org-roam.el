;;;;; Org Roam

(use-package org-roam
  :ensure
  (org-roam :files (:defaults "extensions/*"))
  :after
  (org)
  :custom
  (org-roam-database-connector 'sqlite-builtin "Use built-in sqlite")
  (org-roam-directory
   (file-name-as-directory (expand-file-name "roam" org-directory)) "Set org-roam directory")
  (org-roam-db-gc-threshold most-positive-fixnum "Limit GC during sync")
  :bind
  (:prefix-map as/org-roam-prefix-map
               :prefix "C-c r")
  (:map as/org-roam-prefix-map
        ("a" . org-roam-alias-add)
        ("A" . org-roam-alias-remove)
        ("b" . org-roam-buffer-display-dedicated)
        ("c" . org-roam-capture)
        ("f" . org-roam-node-find)
        ("i" . org-roam-node-insert)
        ("s" . org-roam-db-sync))
  :config
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . right)
                 (window-width . 0.33)
                 (window-height . fit-window-to-buffer)))
  (add-to-list 'org-roam-capture-templates
               '("r" "reference" plain
                 "%?"
                 :target
                 (file+head "references/${citar-citekey}.org"
                            "#+title: ${note-title}\n")
                 :empty-lines 1
                 :unnarrowed t
                 ))
  (add-to-list 'org-roam-capture-templates
               `("p" "project" plain (file ,(concat org-roam-directory
                                                    "/templates/projects.org"))
                 :target
                 (file+head "projects/%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}\n#+filetags: :projects:")
                 :empty-lines 1
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               `("j" "job" plain (file ,(concat org-roam-directory
                                                "/templates/job_applications.org"))
                 :target
                 (file+head "projects/job_applications/%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}\n#+filetags: :job_search:")
                 :empty-lines 1
                 :unnarrowed t))
  (org-roam-db-autosync-mode))

;;;;;; Org Roam UI

(use-package org-roam-ui
  :if
  (elpaca-installed-p 'org-roam)
  :after
  (org-roam)
  :custom
  (org-roam-ui-follow nil "Don't update UI when point moves")
  :bind
  (:map as/org-roam-prefix-map
        ("u" . org-roam-ui-open)))

;;;;;; Citar + Roam

(use-package citar-org-roam
  :if
  (and (elpaca-installed-p 'citar)
       (elpaca-installed-p 'org-roam))
  :after
  (citar org-roam)
  :custom
  (citar-org-roam-capture-template-key "r" "Define which template to use")
  (citar-org-roam-note-title-template "${title} by ${author}" "Default note title")
  (citar-org-roam-subdir "references" "Default note directory")
  :config
  ;; NOTE: Trying `citar-denote'
  ;; (citar-org-roam-mode)
  )
