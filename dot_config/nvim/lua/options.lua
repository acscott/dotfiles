vim.o.encoding = "utf-8"
vim.o.fileencoding = "utf-8"
vim.o.mouse = "a"
vim.o.hidden = true
vim.o.ruler = true
vim.o.cmdheight = 2
vim.o.splitbelow = true
vim.o.splitright = true
vim.o.conceallevel = 0
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.smarttab = true
vim.o.expandtab = true
vim.o.smartindent = true
vim.o.autoindent = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.laststatus = 0
vim.o.number = false
vim.o.wrap = true
vim.o.breakindent = true
vim.o.cursorline = false
vim.o.background = "dark"
vim.o.showtabline = 2
vim.o.updatetime = 300
vim.o.timeoutlen = 500
vim.o.autochdir = true
vim.o.swapfile = true

