#!/bin/sh

# Important! Must update PITA D-Bus
riverctl spawn 'dbus-update-activation-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river'

# Do some logging
#exec 3>&1 4>&2
#trap 'exec 2>&4 1>&3' 0 1 2 3
#exec 1>"${XDG_STATE_HOME}"/river.log 2>&1

#Start Emacs
riverctl map normal Super E spawn emacs

# Start kitty
riverctl map normal Super Return spawn alacritty

# Start fuzzel
riverctl map normal Super Space spawn fuzzel

# Lock screen
riverctl map normal Control+Alt L spawn 'waylock-launcher'

# Logout
riverctl map normal Super End spawn 'wleave -p layer-shell'

# Screenshot entire display to clipboard
riverctl map normal Control Print spawn 'grim -t png - | wl-copy -t image/png'

# Screenshot entire display and save to file
riverctl map normal Super+Control Print spawn 'grim -t png ~/Pictures/screenshot-$(date +%Y-%m-%d_%T).png'

# Screenshot selection to clipboard
riverctl map normal Alt Print spawn 'grim -t png -g "$(slurp -d)" - | wl-copy -t image/png'

# Screenshot selection and save to file
riverctl map normal Super+Alt Print spawn 'grim -t png -g "$(slurp -d)" ~/Pictures/screenshot-$(date +%Y-%m-%d_%T).png'

# Close the focused view
riverctl map normal Super Q close

# Exit river
riverctl map normal Super+Shift E exit

# Emacs-like keybinds for layouts/views

# Focus the next/previous view in the layout stack
riverctl map normal Super N focus-view next
riverctl map normal Super P focus-view previous

# Swap the focused view with the next/previous view in the layout stack
riverctl map normal Super+Alt N swap next
riverctl map normal Super+Alt P swap previous

# Switch back and forth between two tag groups
riverctl map normal Super O focus-previous-tags

# Focus the next/previous output
riverctl map normal Super V focus-output next
riverctl map normal Super+Alt V focus-output previous

# Send the focused view to the next/previous output
riverctl map normal Super Grave send-to-output next
riverctl map normal Super+Shift Grave send-to-output previous

# Bump the focused view to the top of the layout stack
riverctl map normal Super+Shift Return zoom

# Increase/decrease the main ratio of rivertile(1)
riverctl map normal Super B send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Super F send-layout-cmd rivertile "main-ratio +0.05"

# Increment/decrement the main count of rivertile(1)
riverctl map normal Super+Alt F send-layout-cmd rivertile "main-count +1"
riverctl map normal Super+Alt B send-layout-cmd rivertile "main-count -1"

# Snap views to screen edges
riverctl map normal Super+Alt+Control B snap left
riverctl map normal Super+Alt+Control F snap right
riverctl map normal Super+Alt+Control P snap up
riverctl map normal Super+Alt+Control N snap down

# Resize views
riverctl map normal Super+Alt+Shift B resize horizontal  -100
riverctl map normal Super+Alt+Shift F resize horizontal   100
riverctl map normal Super+Alt+Shift P resize vertical    -100
riverctl map normal Super+Alt+Shift N resize vertical     100

# Move views
riverctl map normal Super+Alt+Control+Shift B move left   100
riverctl map normal Super+Alt+Control+Shift F move right  100
riverctl map normal Super+Alt+Control+Shift P move up     100
riverctl map normal Super+Alt+Control+Shift N move down   100

# Change layout
riverctl map normal Super+Shift B send-layout-cmd rivertile "main-location left"
riverctl map normal Super+Shift F send-layout-cmd rivertile "main-location right"
riverctl map normal Super+Shift P send-layout-cmd rivertile "main-location top"
riverctl map normal Super+Shift N send-layout-cmd rivertile "main-location bottom"

## Vim-like keybinds for layouts/views

# Focus the next/previous view in the layout stack
riverctl map normal Super J focus-view next
riverctl map normal Super K focus-view previous

# Swap the focused view with the next/previous view in the layout stack
riverctl map normal Super+Alt J swap next
riverctl map normal Super+Alt K swap previous

# Focus the next/previous output
riverctl map normal Super Period focus-output next
riverctl map normal Super Comma focus-output previous

# Send the focused view to the next/previous output
riverctl map normal Super+Shift Period send-to-output next
riverctl map normal Super+Shift Comma send-to-output previous

# Increase/decrease the main ratio of rivertile(1)
riverctl map normal Super H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Super L send-layout-cmd rivertile "main-ratio +0.05"

# Increment/decrement the main count of rivertile(1)
riverctl map normal Super+Alt L send-layout-cmd rivertile "main-count +1"
riverctl map normal Super+Alt H send-layout-cmd rivertile "main-count -1"

# Snap views to screen edges
riverctl map normal Super+Alt+Control H snap left
riverctl map normal Super+Alt+Control L snap right
riverctl map normal Super+Alt+Control K snap up
riverctl map normal Super+Alt+Control J snap down

# Resize views
riverctl map normal Super+Alt+Shift H resize horizontal  -100
riverctl map normal Super+Alt+Shift J resize horizontal   100
riverctl map normal Super+Alt+Shift K resize vertical    -100
riverctl map normal Super+Alt+Shift L resize vertical     100

# Move views
riverctl map normal Super+Alt+Control+Shift H move left   100
riverctl map normal Super+Alt+Control+Shift L move right  100
riverctl map normal Super+Alt+Control+Shift K move up     100
riverctl map normal Super+Alt+Control+Shift J move down   100

# Change layout
riverctl map normal Super+Shift H send-layout-cmd rivertile "main-location left"
riverctl map normal Super+Shift L send-layout-cmd rivertile "main-location right"
riverctl map normal Super+Shift K send-layout-cmd rivertile "main-location top"
riverctl map normal Super+Shift J send-layout-cmd rivertile "main-location bottom"

# Move views with mouse
riverctl map-pointer normal Super BTN_LEFT move-view

# Resize views with mouse
riverctl map-pointer normal Super BTN_RIGHT resize-view

# Toggle float with mouse
riverctl map-pointer normal Super BTN_MIDDLE toggle-float

for i in $(seq 1 9)
do
    tags=$((1 << (i - 1)))

    # Super+[1-9] to focus tag [0-8]
    riverctl map normal Super "$i" set-focused-tags $tags

    # Super+Shift+[1-9] to tag focused view with tag [0-8]
    riverctl map normal Super+Shift "$i" set-view-tags $tags

    # Super+Control+[1-9] to toggle focus of tag [0-8]
    riverctl map normal Super+Control "$i" toggle-focused-tags $tags

    # Super+Shift+Control+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal Super+Shift+Control "$i" toggle-view-tags $tags
done

# Super+0 to focus all tags
# Super+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 32) - 1))
riverctl map normal Super 0 set-focused-tags $all_tags
riverctl map normal Super+Shift 0 set-view-tags $all_tags

# Toggle float
riverctl map normal Super S toggle-float

# Toggle fullscreen
riverctl map normal Super T toggle-fullscreen

# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

# Enter passthrough mode
riverctl map normal Super F11 enter-mode passthrough

# Return to normal mode
riverctl map passthrough Super F11 enter-mode normal

# Extend locked mode for FUBAR situations
riverctl map locked Super L spawn 'river-exit logout'
riverctl map locked Super R spawn 'river-exit reboot'
riverctl map locked Super P spawn 'river-exit poweroff'

# Function/Media keys
for mode in normal locked
do

  # Volume
  riverctl map "$mode" None XF86AudioMute        spawn 'pactl set-sink-mute @DEFAULT_SINK@ toggle'
  riverctl map "$mode" None XF86AudioLowerVolume spawn 'pactl set-sink-volume @DEFAULT_SINK@ -2%'
  riverctl map "$mode" None XF86AudioRaiseVolume spawn 'pactl set-sink-volume @DEFAULT_SINK@ +2%'
  riverctl map "$mode" None XF86AudioMicMute     spawn 'pactl set-source-mute @DEFAULT_SOURCE@ toggle'

  # Media
  riverctl map "$mode" None XF86AudioPrev spawn ''
  riverctl map "$mode" None XF86AudioPlay spawn ''
  riverctl map "$mode" None XF86AudioNext spawn ''

  # Backlight
  riverctl map $mode None XF86MonBrightnessDown spawn 'xbacklight -dec 5'
  riverctl map $mode None XF86MonBrightnessUp   spawn 'xbacklight -inc 5'

  # Display
  #riverctl map $mode None XF86Display spawn ''

  # WiFi
  riverctl map $mode None XF86WLAN spawn 'toggle-wifi'

  # Notifications
  #riverctl map $mode None XF86NotificationCenter spawn ''

  # Phone
  #riverctl map $mode None XF86PickupPhone spawn ''
  #riverctl map $mode None XF86HangupPhone spawn ''

  # Favorites
  #riverctl map $mode None XF86Favorites spawn ''

done

# Set background and border
riverctl background-color 0x323d43
riverctl border-color-focused 0xd8caac
riverctl border-color-unfocused 0x525c62
riverctl border-color-urgent 0xe68183
riverctl border-width 1

# Keyboard repeat rate
riverctl set-repeat 50 300
{{- if eq .chezmoi.hostname "helix" }}

# Touchpad
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad accel-profile adaptive
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad pointer-accel .5
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad drag enabled
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad disable-while-typing enabled
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad middle-emulation enabled
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad tap enabled
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad tap-button-map left-right-middle
riverctl input pointer-2-7-SynPS/2_Synaptics_TouchPad scroll-method two-finger

# Lid switch
riverctl map-switch normal lid close 'waylock-launcher'
{{- end }}

# Initial tags: $((1 << ("WORKSPACE" - 1)))
riverctl rule-add -app-id 'firefox' tags 1
riverctl rule-add -app-id 'chromium' tags 2
riverctl rule-add -app-id 'thunar' tags $((1 << 2))
riverctl rule-add -app-id 'evince' tags $((1 << 3))
riverctl rule-add -app-id 'libreoffice*' tags $((1 << 3))
riverctl rule-add -app-id 'vivaldi-stable' tags $((1 << 4))
riverctl rule-add -app-id 'Alacritty' tags $((1 << 5))
riverctl rule-add -app-id 'foot' tags $((1 << 5))
riverctl rule-add -app-id 'kitty' tags $((1 << 5))
riverctl rule-add -app-id 'emacs' tags $((1 << 6))
riverctl rule-add -app-id 'kitty' -title 'weechat' tags $((1 << 7))
riverctl rule-add -app-id 'Mattermost' tags $((1 << 7))
riverctl rule-add -app-id 'discord' tags $((1 << 7))
riverctl rule-add -app-id 'Spotify' tags $((1 << 8))
riverctl rule-add -app-id 'thunderbird' tags $((1 << 8))
riverctl rule-add -app-id 'tutanota-desktop' tags $((1 << 8))

{{ if eq .chezmoi.hostname "hydrus" -}}
# Initial display
riverctl rule-add -app-id 'Alacritty' output 'DP-2'
riverctl rule-add -app-id 'chromium' output 'DP-1'
riverctl rule-add -app-id 'discord' output 'DP-2'
riverctl rule-add -app-id 'emacs' output 'DP-2'
riverctl rule-add -app-id 'evince' output 'DP-1'
riverctl rule-add -app-id 'firefox' output 'DP-1'
riverctl rule-add -app-id 'foot' output 'DP-2'
riverctl rule-add -app-id 'kitty' output 'DP-2'
riverctl rule-add -app-id 'libreoffice*' output 'DP-1'
riverctl rule-add -app-id 'kitty' -title 'weechat' output 'DP-1'
riverctl rule-add -app-id 'Mattermost' output 'DP-1'
riverctl rule-add -app-id 'Spotify' output 'DP-1'
riverctl rule-add -app-id 'thunar' output 'DP-1'
riverctl rule-add -app-id 'thunderbird' output 'DP-2'
riverctl rule-add -app-id 'tutanota-desktop' output 'DP-2'

# Starting monitor and tags
riverctl focus-output 'DP-2'
riverctl set-focused-tags $((1 << 6))
{{- end }}

# Initial float
riverctl rule-add -app-id 'Bitwarden' float
riverctl rule-add -app-id 'Blueman-manager' float
riverctl rule-add -app-id 'connman-gtk' float
riverctl rule-add -app-id 'galculator' float
riverctl rule-add -app-id 'org.keepassxc.KeePassXC' float
riverctl rule-add -app-id 'mousepad' float
riverctl rule-add -app-id 'com.nextcloud.desktopclient.nextcloud' float
riverctl rule-add -app-id 'Proton Pass' float
riverctl rule-add -app-id 'vlc' float

# Initial dimensions
riverctl rule-add -add-id 'firefox' -title 'Save as' dimensions 800 600

# Layout generator
riverctl default-layout rivertile
rivertile -view-padding 4 -outer-padding 4 -main-ratio .55 &

# Autostart
riverctl spawn '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1'
riverctl spawn 'kanshi'
riverctl spawn 'idle-launcher'
riverctl spawn 'pipewire-launcher'
# ssh-agent-launcher &
riverctl spawn 'yambar-launcher'
riverctl spawn 'dunst'
riverctl spawn 'gammastep -r'
riverctl spawn 'river-tag-overlay'
riverctl spawn 'swaybg -m fill -i ~/Pictures/Wallpapers/dark-forest-path.jpg'
riverctl spawn 'nextcloud --background'
riverctl spawn 'alacritty'
riverctl spawn 'emacs'
