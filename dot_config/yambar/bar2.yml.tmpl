# Colors
.catppuccin_mocha:
  0: &foreground cdd6f4ff
  1: &background 1e1e2edd
  2: &black 45475aff
  3: &dark_grey 45475aff
  4: &dark_red f38ba8ff
  5: &red f38ba8ff
  6: &dark_green a6e3a1ff
  7: &green a6e3a1ff
  8: &dark_yellow f9e2afff
  9: &yellow f9e2afff
  10: &dark_blue 89b4faff
  11: &blue 89b4faff
  12: &dark_magenta f5c2e7ff
  13: &magenta f5c2e7ff
  14: &dark_cyan 94e2d5ff
  15: &cyan 94e2d5ff
  16: &light_grey bac2deff
  17: &white a6adc8ff
transparent_light_grey: &transparent_light_grey bac2deaa
transparent_black: &transparent_black 45475aaa
# .everforest:
#   0:  &foreground   d8caacff
#   1:  &background   323d43dd
#   2:  &black        4a555bff
#   3:  &dark_grey    525c62ff
#   4:  &dark_red     e26c6eff
#   5:  &red          e68183ff
#   6:  &dark_green   9bb86fff
#   7:  &green        a7c080ff
#   8:  &dark_yellow  d5b26bff
#   9:  &yellow       dbbc7fff
#   10: &dark_blue    6eb2a9ff
#   11: &blue         7fbbb3ff
#   12: &dark_magenta cf87a9ff
#   13: &magenta      d699b6ff
#   14: &dark_cyan    72b783ff
#   15: &cyan         83c092ff
#   16: &light_grey   d0bf9bff
#   17: &white        d8caacff
# transparent_light_grey: &transparent_light_grey d0bf9b99
# transparent_black: &transparent_black 4a555b99
# Fonts
{{- if eq .chezmoi.hostname "helix" }}
hack: &hack Hack:pixelsize=28
awesome: &awesome Font Awesome 6 Free:style=solid:pixelsize=28
{{- else}}
hack: &hack Hack:pixelsize=13
awesome: &awesome Font Awesome 6 Free:style=solid:pixelsize=12
{{- end }}
# Labels
separator:
  &separator { string: { text: "  ", font: *hack, foreground: *light_grey } }

bar:
{{- if eq .chezmoi.hostname "helix" }}
  height: 64
  location: bottom
{{- else if eq .chezmoi.hostname "hydrus" }}
  monitor: DP-2
  height: 28
  location: bottom
{{- else }}
  height: 36
  location: bottom
{{- end }}
  layer: top
  spacing: 2
  margin: 2
  font: *hack
  foreground: *foreground
  background: *background
  border:
{{- if eq .chezmoi.hostname "hydrus" }}
    bottom-margin: 4
    left-margin: 8
    right-margin: 8
{{- else }}
    top-margin: 8
    left-margin: 16
    right-margin: 16
{{- end }}
    width: 0
    color: *transparent_light_grey

  left:
    - river:
        anchors:
          - base: &river_base
{{- if eq .chezmoi.hostname "helix" }}
              left-margin: 56
              right-margin: 56
{{- else }}
              left-margin: 22
              right-margin: 22
{{- end }}
              text: "{id}"
        content:
          map:
            on-click:
              left: sh -c "riverctl set-focused-tags $((1 << ({id} - 1)))"
              middle: sh -c "riverctl toggle-view-tags $((1 << ({id} -1)))"
              right: sh -c "riverctl toggle-focused-tags $((1 << ({id} -1)))"
{{- if eq .chezmoi.hostname "helix" }}
            conditions:
              urgent:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *dark_red}}, {border: {size: 6, color: *black}}]}
                  foreground: *black
              focused:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *light_grey}}, {underline: {size: 6, color: *transparent_black}}]}
                  foreground: *dark_grey
              visible && occupied:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *transparent_light_grey}}, {underline: {size: 6, color: *transparent_black}}]}
                  foreground: *light_grey
              visible && ~occupied:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *transparent_light_grey}}, {underline: {size: 6, color: *transparent_black}}]}
                  foreground: *light_grey
              ~visible && occupied:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *dark_grey}}, {underline: {size: 6, color: *transparent_light_grey}}]}
                  foreground: *light_grey
              ~focused && occupied:
                string:
                  <<: *river_base
                  deco: {stack: [{background: {color: *dark_grey}}, {underline: {size: 6, color: *transparent_light_grey}}]}
                  foreground: *light_grey
              id < 10 && ~visible && ~occupied:
                string:
                  <<: *river_base
                  foreground: *light_grey
              id >= 10 && ~visible && ~occupied: {empty: {}}
{{- else }}
            conditions:
              urgent:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *dark_red } },
                          { border: { size: 2, color: *black } },
                        ],
                    }
                  foreground: *black
              focused:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *light_grey } },
                          { underline: { size: 1, color: *transparent_black } },
                        ],
                    }
                  foreground: *dark_grey
              visible && occupied:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *transparent_light_grey } },
                          { underline: { size: 1, color: *transparent_black } },
                        ],
                    }
                  foreground: *light_grey
              visible && ~occupied:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *transparent_light_grey } },
                          { underline: { size: 1, color: *transparent_black } },
                        ],
                    }
                  foreground: *light_grey
              ~visible && occupied:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *dark_grey } },
                          {
                            underline:
                              { size: 1, color: *transparent_light_grey },
                          },
                        ],
                    }
                  foreground: *light_grey
              ~focused && occupied:
                string:
                  <<: *river_base
                  deco:
                    {
                      stack:
                        [
                          { background: { color: *dark_grey } },
                          {
                            underline:
                              { size: 1, color: *transparent_light_grey },
                          },
                        ],
                    }
                  foreground: *light_grey
              id < 10 && ~visible && ~occupied:
                string:
                  <<: *river_base
                  deco:
                    { underline: { size: 1, color: *transparent_light_grey } }
                  foreground: *light_grey
              id >= 10 && ~visible && ~occupied: { empty: {} }
{{ end }}
  center:
    - foreign-toplevel:
        content:
          map:
            conditions:
              activated:
                - string: { text: "{app-id}: {title}" }
              ~activated: { empty: {} }

  right:
{{- if eq .chezmoi.hostname "hydrus" }}
    - label:
        content: *separator
    - network:
        content:
          map:
            default: { empty: {} }
            conditions:
              name >= wlan0 && name <= wlan9:
                map:
                  conditions:
                    ~carrier: { empty: {} }
                    carrier:
                      map:
                        default:
                          - string: { text: , font: *awesome }
                          - string:
                              {
                                text: " {name}: {dl-speed:mb} mb/s down - {ul-speed:mb} mb/s up",
                              }
                        conditions:
                          ipv4 == "":
                            - string: { text: , font: *awesome }
                            - string:
                                {
                                  text: " {name}: {dl-speed:mb} mb/s down / {ul-speed:mb} mb/s up",
                                }
    - network:
        content:
          map:
            default: { empty: {} }
            conditions:
              name >= eth0 && name <= eth9:
                map:
                  conditions:
                    ~carrier: { empty: {} }
                    carrier:
                      map:
                        default:
                          - string: { text: , font: *awesome }
                          - string:
                              {
                                text: " {name}: {dl-speed:mb} mb/s down / {ul-speed:mb} mb/s up",
                              }
                        conditions:
                          state == down: { string: { text: "", font: *awesome } }
                          # state == down: { string: { text: , font: *awesome } }
                          ipv4 == "":
                            - string: { text: , font: *awesome }
                            - string:
                                {
                                  text: " {name}: {dl-speed:mb} mb/s down / {ul-speed:mb} mb/s up",
                                }
    - label:
        content: *separator
{{- end }}
    - cpu:
        poll-interval: 2000
        content:
          map:
            conditions:
              id == -1:
                - string: { text: , font: *awesome }
                - string: { text: " {cpu}%" }
    - label:
        content: *separator
    - mem:
        poll-interval: 5000
        content:
          - string: { text: , font: *awesome }
          - string: { text: " {used:mb} MiB ({percent_used}% Used)" }
